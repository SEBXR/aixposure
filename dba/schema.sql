-- Active: 1683223213044@@127.0.0.1@5432@aixposure@public
CREATE TABLE
    categories(
        id SERIAL PRIMARY KEY,
        name VARCHAR(50)
    );

CREATE TABLE
    articles(
        id SERIAL PRIMARY KEY,
        title VARCHAR(100),
        category_id INTEGER REFERENCES categories(id), --Ty no atao h2
        content TEXT
    );

CREATE TABLE 
    admin(
        id SERIAL PRIMARY KEY,
        username VARCHAR(50),
        password TEXT
    );
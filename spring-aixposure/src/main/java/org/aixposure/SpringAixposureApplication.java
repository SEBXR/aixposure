package org.aixposure;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class SpringAixposureApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringAixposureApplication.class, args);
	}

	@GetMapping("/")
	public String hello(){
		return "yes , it's ducking work";
	}
}

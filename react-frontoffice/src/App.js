import {Routes , Route } from 'react-router-dom' 
import Articles from './components/Articles';
import ArticleAbout from './components/ArticleAbout';
import './App.css'


function App() {
  return (
    <div className="App">
      <Routes>
        <Route path='/' element={<Articles/>}></Route>
        <Route path='/:slug/:id' element={<ArticleAbout/>}></Route>
      </Routes>
    </div>
  );
}

export default App;

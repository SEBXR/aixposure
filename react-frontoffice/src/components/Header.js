import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import { Link } from 'react-router-dom';

function Header() {
  return (
    <Container>
      <Navbar expand="lg" variant="dark" bg="dark">
        <Container>
          <Navbar.Brand ><Link to='/' style={{textDecoration:"none" , color:"white"}}>AIxposure - IA Infos</Link></Navbar.Brand>
        </Container>
      </Navbar>
    </Container>
  );
}

export default Header;
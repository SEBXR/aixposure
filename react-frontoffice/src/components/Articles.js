import { useState, useEffect } from "react";
import Table from 'react-bootstrap/Table';
import Header from "./Header";
import { Link } from "react-router-dom";
import slugify from 'slugify';
// import articles from './articles.json'
import { url } from "./url";

function Articles() {
    const [articles, setArticles] = useState([])
    useEffect(() => {
        fetch(url + "articles")
            .then(response => response.json())
            .then(data => setArticles(data))
            .catch(error => console.error(error));
    }, []);
    console.log(articles)
    if (articles.length == 0) {
        return (
            <div>
                <Header />
                <div className="d-flex mt-5 justify-content-center" >
                    <Table striped bordered hover style={{ width: '800px' }} size="lg">
                        <thead>
                            <tr><h1>Articles 'list</h1></tr>
                        </thead>
                        <tbody>
                            <h1>Loading ...</h1>
                        </tbody>
                    </Table>
                </div>
            </div>
        )
    }
    return (
        <div>
            <Header />
            <div className="d-flex mt-5 justify-content-center" >
                <Table striped bordered hover style={{ width: '800px' }} size="lg">
                    <thead>
                        <tr><h1>Articles 'list</h1></tr>
                    </thead>
                    <tbody>
                        {articles.map(article => (
                            <tr id="{article.id}">
                                <td><strong><Link to={`${slugify(article.title)}/${article.id}`}>{article.title}</Link></strong></td>
                            </tr>
                        ))}
                    </tbody>
                </Table>
            </div>
        </div>
    )
}

export default Articles;
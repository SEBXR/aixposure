import React, { useState, useEffect } from "react";
import Header from "./Header"
import Card from 'react-bootstrap/Card';
import { Link } from "react-router-dom";
import { url } from './url'

export default function ArticleAbout() {
    const uri = window.location.href
    const id = uri[uri.length - 1]
    const [article, setArticle] = useState(null)
    useEffect(() => {
        fetch(url + "findArticle/" + id)
            .then(response => response.json())
            .then(data => setArticle(data))
    }, []);
    if (!article) {
        return (

            <div>
                <Header />
                <h1>Loading ...</h1>
            </div>
        )
    }
    return (
        <div>
            <Header />
            <div className="d-flex mt-5 justify-content-center">
                <Card style={{ width: '35rem' }}>
                    <Card.Body>
                        <Card.Title><h1>{article.title}</h1></Card.Title>
                        <Card.Subtitle className="my-4 "><h3>Category: {article.category.name}</h3></Card.Subtitle>
                        <Card.Text>
                            {React.createElement("div", { dangerouslySetInnerHTML: { __html: article.content } })}
                        </Card.Text>
                        <Link to='/'>Return</Link>
                    </Card.Body>
                </Card>
            </div>
        </div>
    )
}
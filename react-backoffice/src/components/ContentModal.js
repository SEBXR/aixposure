import Modal from 'react-bootstrap/Modal';
import React from 'react';
import Button from 'react-bootstrap/Button';


function ContentModal(props) {
    return (
        <Modal
            show={props.showModal}
            onHide={props.closeModal}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header closeButton>
                <Modal.Title>
                    Content
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {React.createElement("div", { dangerouslySetInnerHTML: { __html: props.selectContent } })}
            </Modal.Body>
            <Modal.Footer>
                <Button onClick={props.closeModal}>Close</Button>
            </Modal.Footer>
        </Modal>
    )
}

export default ContentModal
import 'bootstrap/dist/css/bootstrap.min.css';
import './Login.css'; // custom styles
import Swal from "sweetalert2"
import { url } from './url';

function Login() {
    const handleSubmit = (event) => {
        // just to see if i still can push
        event.preventDefault();
        Swal.fire({
            title: 'Please wait ...',
            icon: 'info',
            showCancelButton: false,
            showConfirmButton: false
        })
        const form = new FormData(event.target)
        const xhr = new XMLHttpRequest()
        xhr.open('POST', url + 'login', true)
        xhr.send(form)
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4 && xhr.status === 200) {
                const response = this.responseText
                if (response == 0) {
                    Swal.fire({
                        title: 'Username or password incorrect !',
                        icon: 'error'
                    }
                    )
                }
                else {
                    localStorage.setItem('id', response)
                    window.location.href = '/articles'
                }
            }
        }

    };

    return (
        <div className="login-page">
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-md-6">
                        <div className="card">
                            <div className="card-header">
                                <h2 className="text-center mb-0">Log In</h2>
                            </div>
                            <div className="card-body">
                                <form onSubmit={handleSubmit}>
                                    <div className="form-group mb-4">
                                        <label for="username" className="font-weight-bold">
                                            Username
                                        </label>
                                        <input type="text" className="form-control" name="username" value="Admin" />
                                    </div>
                                    <div className="form-group mb-4">
                                        <label for="password" className="font-weight-bold">
                                            Password
                                        </label>
                                        <input type="password" className="form-control" name="password" value="1234" />
                                    </div>
                                    <div className="form-group">
                                        <button type="submit" className="btn btn-primary btn-block">Confirm</button>
                                    </div>
                                </form>
                            </div>
                            <div className="card-footer">
                                <strong className="text-center mb-0">AIxposure Back Office</strong>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Login;

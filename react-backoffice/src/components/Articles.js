import Table from 'react-bootstrap/Table'
import Button from 'react-bootstrap/Button';
import Sidebar from './Sidebar';
import { useState, useEffect } from 'react';
import Swal from 'sweetalert2';
import ContentModal from './ContentModal';
import ModifyModal from './ModifyModal';
import { url } from './url'
// import articles from './articles.json'


function Articles() {
    const [articles, setArticles] = useState([])
    useEffect(() => {
        fetch(url + "articles")
            .then(response => response.json())
            .then(data => setArticles(data));
    }, []);

    const [showContentModal, setshowContentModal] = useState(false);
    const [selectContent, setselectContent] = useState(null);

    const openContentModal = (event) => {
        setselectContent(event.target.dataset.content);
        setshowContentModal(true);
    };
    const closeContentModal = () => {
        setselectContent(null);
        setshowContentModal(false);
    };

    const [showModifyModal, setshowModifyModal] = useState(false);
    const [selectArticle, setselectArticle] = useState(null);

    const openModifyModal = (event) => {
        setselectArticle(event.target.dataset.article);
        setshowModifyModal(true);
    };
    const closeModifyModal = () => {
        setselectArticle(null);
        setshowModifyModal(false);
    };

    function deleteArticle(event) {
        Swal.fire({
            title: 'Are you sure ?',
            icon: 'question',
            confirmButtonText: 'Delete',
            confirmButtonColor: '#dc3545',
            showCloseButton: true
        }).then((result) => {
            if (result.isConfirmed) {
                Swal.fire({
                    title: 'Please wait ...',
                    icon: 'info',
                    showCancelButton: false,
                    showConfirmButton: false
                })
                const id = event.target.dataset.id
                const xhr = new XMLHttpRequest()
                xhr.open('POST', url + "delete/" + id, true)
                xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded")
                xhr.send()
                xhr.onreadystatechange = function () {
                    if (xhr.readyState === 4 && xhr.status === 200) {
                        const response = this.responseText
                        if (response == 1)
                            window.location.replace('/articles');
                        else
                            Swal.fire('An error occured', '', 'error')
                    }
                }
            }
        })
    }
    if (articles.length == 0) {
        return (
            <div className='row'>
                <div className='col-2'>
                    <Sidebar />
                </div>
                <div className='col-10 mt-5 mr-5'>
                    <h1>Articles' list</h1>
                    <h1>Loading ...</h1>
                </div>
            </div>
        )
    }
    return (
        <div className='row'>
            <div className='col-2'>
                <Sidebar />
            </div>
            <div className='col-10 mt-5 mr-5'>
                <h1>Articles' list</h1>
                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Title</th>
                            <th>Category</th>
                            <th colSpan={3}>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {articles.map(article => (
                            <tr>
                                <td>{article.id}</td>
                                <td>{article.title}</td>
                                <td>{article.category.name}</td>
                                <td><Button variant='outline-success' onClick={openContentModal} data-content={article.content}>See content</Button></td>
                                <td><Button variant='outline-primary' onClick={openModifyModal} data-article={JSON.stringify(article)}>Modify</Button></td>
                                <td><Button variant='outline-danger' onClick={deleteArticle} data-id={article.id}>Delete</Button></td>
                            </tr>
                        ))}
                        <ContentModal showModal={showContentModal} closeModal={closeContentModal} selectContent={selectContent} />
                        <ModifyModal showModal={showModifyModal} closeModal={closeModifyModal} article={selectArticle} />
                    </tbody>
                </Table>
            </div>
        </div>
    )
}

export default Articles;
import Sidebar from './Sidebar'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import { useState, useEffect } from 'react'
import { CKEditor } from '@ckeditor/ckeditor5-react'
import ClassicEditor from '@ckeditor/ckeditor5-build-classic'
import Swal from 'sweetalert2'
import { url } from './url'
function NewArticle() {
    const [categories, setCategories] = useState([])
    const [content, setContent] = useState('')
    useEffect(() => {
        fetch(url + "categories")
            .then(response => response.json())
            .then(data => setCategories(data));
    }, [])
    const handleSubmit = (event) => {
        event.preventDefault()
        Swal.fire({
            title: 'Please wait ...',
            icon: 'info',
            showCancelButton: false,
            showConfirmButton: false
        })
        const form = new FormData(event.target)
        form.append('content', content)
        const xhr = new XMLHttpRequest()
        xhr.open('POST', url + "create", true)
        xhr.send(form)
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4 && xhr.status === 200) {
                const response = this.responseText
                if (response != null) {
                    Swal.fire({
                        title: 'Your article have been inserted',
                        icon: 'success',
                        showCancelButton: false,
                        showConfirmButton: false
                    }
                    )
                    setTimeout(function () {
                        window.location.href = '/articles';
                    }, 1500
                    );
                }
            }
        }

    }
    return (
        <div className="row">
            <div className='col-2'>
                <Sidebar />
            </div>
            <div className="col-10 mt-5 d-flex justify-content-center">
                <Form className="w-25" onSubmit={handleSubmit}>
                    <h1>Add a new article</h1>
                    <Form.Group className="mb-3" >
                        <Form.Label column="lg" lg={2}>Title</Form.Label>
                        <Form.Control type="text" placeholder="Enter title" name='title' />
                    </Form.Group>

                    <Form.Group className="mb-3">
                        <Form.Label column="lg" lg={2}>Category</Form.Label>
                        <Form.Select name='category'>
                            {categories.map(categorie => (
                                <option value={categorie.id}>{categorie.name}</option>
                            ))}
                        </Form.Select>
                    </Form.Group>

                    <Form.Group className="mb-3" >
                        <Form.Label column="lg" lg={2}>Content</Form.Label>
                        <CKEditor
                            editor={ClassicEditor}
                            onBlur={(event, editor) => {
                                const data = editor.getData()
                                setContent(data)
                            }}
                        />
                    </Form.Group>
                    <Button variant="primary" type="submit">
                        Add
                    </Button>
                </Form>
            </div>
        </div>
    )
}

export default NewArticle
import Modal from 'react-bootstrap/Modal';
import React from 'react';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form'
import { CKEditor } from '@ckeditor/ckeditor5-react'
import ClassicEditor from '@ckeditor/ckeditor5-build-classic'
import { useState } from 'react';
import Swal from 'sweetalert2';
import {url} from './url';


function ModifyModal(props) {
    const [content, setContent] = useState('')
    let article = {
        "content": ""
    }
    if (props.article)
        article = JSON.parse(props.article)
    const handleSubmit = (event) => {
        event.preventDefault();
        Swal.fire({
            title: 'Please wait ...',
            icon: 'info',
            showCancelButton: false,
            showConfirmButton: false
        })
        const form = new FormData(event.target)
        form.append('content', content)
        const xhr = new XMLHttpRequest()
        xhr.open('POST', url + "modify", true)
        xhr.send(form)
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4 && xhr.status === 200) {
                const response = this.responseText
                if (response == 1) {
                    Swal.fire({
                        title: 'The article have been modified ',
                        icon: 'success',
                        showCancelButton: false,
                        showConfirmButton: false
                    })
                    setTimeout(function () {
                        window.location.href = '/articles';
                    }, 1500
                    );
                } else
                    Swal.fire('An error occured', '', 'error')
            }
        }
    };
    return (
        <Modal
            show={props.showModal}
            onHide={props.closeModal}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header closeButton>
                <Modal.Title>
                    Modify an article
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form onSubmit={handleSubmit}>
                    <Form.Control type='hidden' name="id" value={article.id} />
                    <Form.Group className="mb-3" >
                        <Form.Label column="lg" lg={2}>Title</Form.Label>
                        <Form.Control type="text" defaultValue={article.title} name='title' />
                    </Form.Group>

                    <Form.Group className="mb-3" >
                        <Form.Label column="lg" lg={2}>Content</Form.Label>
                        <CKEditor
                            editor={ClassicEditor}
                            data={article.content}
                            onReady={editor => {
                                const data = editor.getData()
                                setContent(data)
                            }}
                            onChange={(event, editor) => {
                                const data = editor.getData()
                                setContent(data)
                            }}
                        />
                    </Form.Group>
                    <Button variant="success" type="submit">
                        Modify
                    </Button>
                </Form>
            </Modal.Body>
            <Modal.Footer>
                <Button onClick={props.closeModal}>Close</Button>
            </Modal.Footer>
        </Modal>
    )
}

export default ModifyModal
import React from 'react';
import {
  CDBSidebar,
  CDBSidebarContent,
  CDBSidebarFooter,
  CDBSidebarHeader,
  CDBSidebarMenu,
  CDBSidebarMenuItem,
} from 'cdbreact';
import { NavLink } from 'react-router-dom';

const Sidebar = () => {
  if (!localStorage.getItem('id')) {
    window.location.href = '/'
  }
  return (
    <div style={{ display: 'flex', height: '100vh', overflow: 'scroll initial' }}>
      <CDBSidebar textColor="#fff" backgroundColor="#333" maxWidth='300px'>
        <CDBSidebarHeader >
          <a href="/articles" className="text-decoration-none" style={{ color: 'inherit' }}>
            Dashboard AIxposure
          </a>
        </CDBSidebarHeader>

        <CDBSidebarContent className="sidebar-content">
          <CDBSidebarMenu>
            <NavLink exact to="/articles" activeClassName="activeClicked">
              <CDBSidebarMenuItem icon="file-alt">Articles' list</CDBSidebarMenuItem>
            </NavLink>
            <NavLink exact to="/new" activeClassName="activeClicked">
              <CDBSidebarMenuItem icon="plus-circle">Add an article</CDBSidebarMenuItem>
            </NavLink>
            <NavLink exact to="/" activeClassName="activeClicked">
              <CDBSidebarMenuItem icon="step-backward">Log Out</CDBSidebarMenuItem>
            </NavLink>
          </CDBSidebarMenu>
        </CDBSidebarContent>

        <CDBSidebarFooter>
          <div
            style={{
              padding: '20px 5px',
            }}
          >
            <p className="text-center mb-0">&copy; 2023 All rights reserved</p>
          </div>
        </CDBSidebarFooter>
      </CDBSidebar>
    </div>
  );
};

export default Sidebar;
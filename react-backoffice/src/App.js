import './App.css';
import Articles from './components/Articles';
import Login from './components/Login';
import {Routes , Route } from 'react-router-dom' 
import NewArticle from './components/NewArticle';

function App() {
  return (
    <div className="App">
      <Routes>
        <Route path='/articles' element={<Articles/>}></Route>
        <Route path='/' element={<Login/>}></Route>
        <Route path='/new' element={<NewArticle/>}></Route>
      </Routes>
    </div>
  );
}

export default App;
